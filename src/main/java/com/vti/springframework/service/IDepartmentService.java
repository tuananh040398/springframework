package com.vti.springframework.service;

import com.vti.springframework.modal.dto.DepartmentRequest;
import com.vti.springframework.modal.entity.Department;

import java.util.List;

public interface IDepartmentService {
    List<Department> getAllDepartment();

    List<Department> search(DepartmentRequest request);

    Department getById(int id);

    void delete(int id);
}
