package com.vti.springframework.service.impl;

import com.vti.springframework.modal.dto.DepartmentRequest;
import com.vti.springframework.modal.entity.Department;
import com.vti.springframework.modal.entity.TypeDepartment;
import com.vti.springframework.repository.DepartmentRepository;
import com.vti.springframework.service.IDepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional(rollbackOn = Exception.class)
public class DepartmentService implements IDepartmentService {

    @Autowired
    private DepartmentRepository repository;

    @Override
    public List<Department> getAllDepartment() {
        return repository.findAll();
    }

    @Override
    public List<Department> search(DepartmentRequest request) {
        String name = "%" + request.getName() + "%";
        int min = request.getMinTotalMember();
        int max = request.getMaxTotalMember();
        TypeDepartment typeDepartment = request.getTypeDepartment();
        return repository.searchV1(name, min, max, typeDepartment);
    }


    @Override
    public Department getById(int id) {
        Optional<Department> optional = repository.findById(id);
        return optional.orElse(null);
    }

    @Override
    public void delete(int id) {
        if (repository.findById(id).isPresent()){
            repository.deleteById(id);
        }else {
            throw new RuntimeException("Department không tồn tại");
        }
    }
}
