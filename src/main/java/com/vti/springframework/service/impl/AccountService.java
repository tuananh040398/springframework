package com.vti.springframework.service.impl;

import com.vti.springframework.modal.entity.Account;
import com.vti.springframework.repository.AccountRepository;
import com.vti.springframework.service.IAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AccountService implements IAccountService {
    @Autowired
    private AccountRepository repository;

    @Override
    public List<Account> getAllAccount() {
        return repository.findAll();
    }

    @Override
    public Account getById(int id) {
        // Optional là 1 dạng đối tượng chống null...
        Optional<Account> optionalAccount = repository.findById(id);
        if (optionalAccount.isPresent()) {
            return optionalAccount.get();
        } else {
            return null;
        }
    }

    @Override
    public Account update(int id, Account account) {
        return null;
    }

    @Override
    public void create(Account account) {

    }

    @Override
    public void delete(int id) {
        if(repository.findById(id).isEmpty()) {
            throw  new RuntimeException("Account không tồn tại");
        }
        repository.deleteById(id);
    }

    @Override
    public List<Account> searchByName(String name, String email) {
        return repository.findAllByUsernameContainsAndEmailContains(name, email);
    }
}
