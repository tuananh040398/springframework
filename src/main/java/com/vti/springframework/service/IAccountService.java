package com.vti.springframework.service;

import com.vti.springframework.modal.entity.Account;

import java.util.List;

public interface IAccountService {
    List<Account> getAllAccount();

    Account getById(int id);

    Account update(int id, Account account);

    void create(Account account);

    void delete(int id);

    List<Account> searchByName(String name, String email);
}
