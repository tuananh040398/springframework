package com.vti.springframework.controller;

import com.vti.springframework.modal.entity.Account;
import com.vti.springframework.service.impl.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/account")
public class AccountController {
    @Autowired
    private AccountService service;

    @GetMapping("/search")
    public List<Account> getAllAccount() {
        return service.getAllAccount();
    }

    @GetMapping("/{id}")
    public Account getByIdV1(@PathVariable int id) {
        return service.getById(id);
    }

    @DeleteMapping("/{id}")
    public String deleteById(@PathVariable int id) {
        try {
            service.delete(id);
            return "Xóa thành công";
        } catch (Exception e) {
            return e.getMessage();
        }
    }

    @GetMapping("/searchbyname")
    public List<Account> getAllAccountByName(@RequestParam String username, @RequestParam String email) {
        return service.searchByName(username, email);
    }
}
