package com.vti.springframework.controller;

import com.vti.springframework.modal.dto.DepartmentRequest;
import com.vti.springframework.modal.entity.Department;
import com.vti.springframework.service.impl.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/department")
public class DepartmentController {
    @Autowired
    private DepartmentService service;

    @GetMapping("/get-all")
    public List<Department> getAllDepartment(){
        return  service.getAllDepartment();
    }

    @PostMapping("/search")
    public List<Department> search(@RequestBody DepartmentRequest request){
        return service.search(request);
    }

    @GetMapping("/{id}")
    public Department getById(@PathVariable int id){
        return service.getById(id);
    }

    @DeleteMapping("/{id}")
    public String deleteById(@PathVariable int id){
        try {
            service.delete(id);
            return "Xóa thành công.";
        } catch (Exception e) {
            return e.getMessage();
        }
    }

}
