package com.vti.springframework.utils;

import com.vti.springframework.modal.entity.Account;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

public class HibernateUtils {
    private  SessionFactory sessionFactory;

    public  SessionFactory buildSessionFactory() {
        if (sessionFactory == null) {
            Configuration configuration = configure();
            ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
                    .applySettings(configuration.getProperties()).build();
            return configuration.buildSessionFactory(serviceRegistry);
        }

        return sessionFactory;
    }

    private  Configuration configure(){
        //Load Configuration
        Configuration configuration = new Configuration();
        configuration.configure("hibernate.cfg.xml");
        //add Entity
        configuration.addAnnotatedClass(Account.class);
        return configuration;
    }

    public  void closeFatory(){
        if (sessionFactory.isClosed()){
            sessionFactory.close();
        }
    }

    public  Session openSession(){
        return buildSessionFactory().openSession();
    }
}
