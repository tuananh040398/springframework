package com.vti.springframework.modal.entity;

import lombok.*;

import javax.persistence.*;
import java.util.Date;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
@Table(name = "Department")
public class Department {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "name", unique = true, nullable = false, length = 50)
    private String name;

    @Column(name = "total_member")
    private int totalMember;

    @Column(name = "type")
    @Enumerated(EnumType.STRING)
    private TypeDepartment type;

    @Column(name = "created_date")
    private Date createdDate;

}
