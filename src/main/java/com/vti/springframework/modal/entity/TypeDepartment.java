package com.vti.springframework.modal.entity;

public enum TypeDepartment {
    Dev, Test, ScrumMaster, PM
}
