package com.vti.springframework.modal.dto;

import com.vti.springframework.modal.entity.TypeDepartment;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class DepartmentRequest {
    private String name;
    private int minTotalMember;
    private int maxTotalMember;
    private TypeDepartment typeDepartment;

}
