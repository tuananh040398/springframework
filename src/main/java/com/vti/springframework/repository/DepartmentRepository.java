package com.vti.springframework.repository;

import com.vti.springframework.modal.entity.Department;
import com.vti.springframework.modal.entity.TypeDepartment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DepartmentRepository extends JpaRepository<Department, Integer> {
//    Search Department theo tên method
    List<Department> findAllByNameContainingAndTotalMemberBetweenAndTypeEquals(String name, int minTotalMember, int maxTotalMember, TypeDepartment type);

//    Cách 2: Search Department theo câu lệnh HQL
    @Query(value = "SELECT dp from Department dp " +
            "WHERE dp.name like :name and dp.totalMember between :minTotalMember AND :maxTotalMember AND dp.type = :type")
    List<Department> searchV1(String name, int minTotalMember, int maxTotalMember, TypeDepartment type);

//    Cách 2.2: Search Department theo câu lệnh SQL
//    @Query(value = "SELECT * from Department dp " +
//        "WHERE dp.name like :name and dp.total_member between :minTotalMember AND :maxTotalMember AND dp.type = :type", nativeQuery = true)
//    List<Department> searchV2(String name, int minTotalMember, int maxTotalMember, String type);

}
